"""init migration

Revision ID: 146220437d33
Revises: 
Create Date: 2021-07-20 14:29:14.915985

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '146220437d33'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('user',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('username', sa.String(length=64), nullable=True),
    sa.Column('uuid', sa.String(length=48), nullable=True),
    sa.Column('active', sa.Boolean(), nullable=True),
    sa.Column('password_hash', sa.String(length=128), nullable=True),
    sa.Column('permissions', sa.Integer(), nullable=True),
    sa.Column('access_token', sa.String(length=32), nullable=True),
    sa.Column('server_id', sa.String(length=48), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_user_username'), 'user', ['username'], unique=True)
    op.create_index(op.f('ix_user_uuid'), 'user', ['uuid'], unique=True)
    op.create_table('ely_data',
    sa.Column('id', sa.Integer(), autoincrement=False, nullable=False),
    sa.Column('uuid', sa.String(length=48), nullable=True),
    sa.Column('username', sa.String(length=64), nullable=True),
    sa.Column('access_token', sa.String(length=300), nullable=True),
    sa.Column('profile_link', sa.String(length=30), nullable=True),
    sa.Column('last_updated', sa.DateTime(), nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_ely_data_uuid'), 'ely_data', ['uuid'], unique=True)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_ely_data_uuid'), table_name='ely_data')
    op.drop_table('ely_data')
    op.drop_index(op.f('ix_user_uuid'), table_name='user')
    op.drop_index(op.f('ix_user_username'), table_name='user')
    op.drop_table('user')
    # ### end Alembic commands ###
