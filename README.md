# Flask Cabinet

Минималистичный кабинет для лаунчера gravit, использующий систему Ely

Этот проект использует:
- [Flask](https://flask.palletsprojects.com/)
- [Bootstrap](https://getbootstrap.com/)

### Настройка:

- Создайте venv
- Установите пакеты из requirements.txt (если необходимо, доставьте нужный пакет для работы с вашей базой данных)
- Создайте файл .env и переопределите переменные:
- - **SECRET_KEY** — секретный ключ, используемый модулями Flask, может быть любой случайной строкой
- - **ELYBY_CLIENT_ID** и **ELYBY_CLIENT_SECRET** — получите их в [кабинете разработчика Ely](https://account.ely.by/dev/)
- - **REDIRECT_URI** — ссылка редиректа для Ely OAuth2
- - **SQLALCHEMY_DATABASE_URI** — строка подключения к базе данных

TODO: дописать про настройку базы (Миграции пока что не публикую, пока ковыряю модели)


### Структура:

`flask_mc_cabinet` - основной пакет
- `public` - основная часть кабинета
- `ely` - модуль регистрации/логина через Ely
- `gravit` - модуль для работы с gravit

### Пример конфигурации Gravit:

Замените URL на ваш эндпоинт

TODO: доделать path скинов

```
  "auth": {
    "std": {
      "isDefault": true,
      "provider": {
        "type": "json",
        "url": "URL/provider",
        "apiKey": "lol"
      },
      "handler": {
        "type": "json",
        "getUrl": "URL/handler/getUrl",
        "updateAuthUrl": "URL/handler/updateAuthUrl",
        "updateServerIdUrl": "URL/handler/updateServerIdUrl",
        "apiKey": "lol"
      },
      "textureProvider": {
        "skinURL": "http://example.com/skins/%username%.png",
        "cloakURL": "http://example.com/cloaks/%username%.png",
        "type": "request"
      },
      "displayName": "Default"
    } 
  }
```

### TODO:

- [_] Доработать дизайн
- [_] Добавить поддержку других систем?
- [_] Реструктуризация приложения для более опрятного вида
- [_] Сделать модули отключаемыми
- [_] Форма смены пароля
- [_] Безпарольная авторизация в лаунчере через токены на сайте
- [_] Другие системы и/или возможность регистрации напрямую (+ хранилище/прокси скинов)
- [_] Закинуть базовую конфигурацию Nginx, systemd-юнит
