import os
import shutil
from dotenv import load_dotenv
from flask import Flask

from environs import EnvError

from flask_mc_cabinet import public, ely, gravit, commands
from flask_mc_cabinet.main_config import Config
from flask_mc_cabinet.extensions import db, migrate, login_manager

from flask_mc_cabinet.public.models import User, ElyData


def create_app(instance_path=None):
    app = Flask(
        __name__, instance_path=instance_path, instance_relative_config=True
    )
    prepare_instance_path(app)
    register_config(app)
    register_extensions(app)
    register_blueprints(app)
    register_shellcontext(app)
    register_commands(app)

    return app


def prepare_instance_path(app):
    print(app.instance_path)
    if not os.path.exists(app.instance_path):
        os.mkdir(app.instance_path)
    if not os.path.exists(os.path.join(app.instance_path, ".env")):
        shutil.copyfile(
            os.path.join(
                os.path.abspath(os.path.dirname(__file__)), "dotenv_empty"
            ),
            os.path.join(app.instance_path, ".env"),
        )


def register_config(app):
    load_dotenv(os.path.join(app.instance_path, ".env"))
    try:
        app.config.from_object(Config(app.instance_path))
    except EnvError as e:
        raise RuntimeError(
            "\n\033[93m"
            "Looks like you're running this app for the first time, "
            "or you forgot to configure few values"
            "\033[0m\n"
            f"Raised error: \n\033[93m{e}\033[0m\n"
            "Please reconfigure settings in "
            f"\033[1m{os.path.join(app.instance_path, '.env')}\033[0m\n"
            "Also you might need to configure database"
        )


def register_blueprints(app: Flask):
    app.register_blueprint(public.views.blueprint)
    app.register_blueprint(ely.views.bp_ely)
    app.register_blueprint(gravit.views.bp_gravit)


def register_extensions(app):
    db.init_app(app)
    migrate.init_app(app, db)
    login_manager.init_app(app)


def register_shellcontext(app):
    """Register shell context objects."""

    def shell_context():
        """Shell context objects."""
        return {"db": db, "User": User, "ElyData": ElyData}

    app.shell_context_processor(shell_context)


def register_commands(app):
    app.cli.add_command(commands.check_app)
