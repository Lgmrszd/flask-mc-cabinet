from datetime import datetime
from flask import Blueprint, request, url_for, flash, render_template
from flask_login import current_user, login_user, login_required
from werkzeug.utils import redirect

from flask_mc_cabinet.extensions import db
from flask_mc_cabinet.public.models import User, ElyData

from .processing import obtain_token, get_raw_data
from .forms import RegisterForm

bp_ely = Blueprint(
    "ely", __name__, url_prefix="/ely", template_folder="templates"
)


@bp_ely.route("/oauth")
def oauth():
    # TODO: change so you can rebind to existing account
    if current_user.is_authenticated:
        flash("You are already logged in!", category="error")
        return redirect(url_for("public.index"))
    error = request.args.get("error")
    if error:
        return "Error!"
    code = request.args.get("code")
    access_token = obtain_token(code)
    if access_token:
        raw_data = get_raw_data(access_token)
        if raw_data.get("status"):
            return (
                "Error: "
                f"status {raw_data['status']}, message: {raw_data['message']}"
            )
        ely_data: ElyData = ElyData.query.filter_by(id=raw_data["id"]).first()

        if ely_data and ely_data.user and ely_data.user.active:
            # We have both data and associated user, proceed with log in
            # TODO: possible checks?
            # Username/UUID changed? Do them in login view?
            # new_uri = url_for("ely.login", ely_data_id = ely_data.id)
            ely_data.access_token = access_token
            ely_data.username = raw_data["username"]
            ely_data.uuid = raw_data["uuid"]
            ely_data.last_updated = datetime.now()
            db.session.add(ely_data)
            db.session.commit()
            login_user(ely_data.user)
            flash("Logged in using Ely!", category="message")
            return redirect(url_for("public.index"))

        elif ely_data:
            # User does not exists or is not active, erase data as non-valid
            if ely_data.user:
                db.session.delete(ely_data.user)
            db.session.delete(ely_data)
            db.session.commit
            del ely_data

        # If we never saw profile with this ID that means this is a new user
        ely_data = ElyData.from_raw_data(raw_data)
        ely_data.access_token = access_token
        ely_data.user = User(username=ely_data.username)
        db.session.add(ely_data)
        db.session.add(ely_data.user)
        db.session.commit()
        login_user(ely_data.user)

        return redirect(url_for("ely.register"))


# TODO: is this even needed?
# @bp_ely.route("/login/<int:ely_data_id>")
# def login(ely_data_id):
#     if current_user.is_authenticated:
#         flash("You are already logged in!")
#         return redirect(url_for("public.index"))
#     ely_data = ElyData.query.filter_by(id=ely_data_id).first_or_404()
#     user = ely_data.user
#     login_user(user)
#     # TODO: additional checks if Ely info changed?
#     flash(f"Logged in as {user.username}")
#     return redirect(url_for("public.index"))


@bp_ely.route("/register", methods=["GET", "POST"])
@login_required
def register():
    if current_user.active:
        # TODO: do something if account is active
        flash("Already active account!", category="error")
        return redirect(url_for("public.index"))
    user: User = current_user
    ely_data = user.ely_data
    form = RegisterForm()
    if form.validate_on_submit():
        user.active = True
        user.uuid = ely_data.uuid
        user.password_hash = "INVALID:Registered Through OAuth"
        db.session.add(user)
        db.session.commit()
        flash("Registered!", category="message")
        return redirect(url_for("public.index"))
    fields = {
        "username": ely_data.username,
        "uuid": ely_data.uuid,
        "profile_link": ely_data.profile_link,
        "active": "Yes" if current_user.active else "No",
    }
    return render_template("ely_register.html", form=form, **fields)
