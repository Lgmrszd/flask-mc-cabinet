import requests
from flask import current_app

# from flask_mc_cabinet.public.models import ElyData


def obtain_token(code):
    token_uri = "https://account.ely.by/api/oauth2/v1/token"
    # token_uri = "https://dev.account.ely.by/api/oauth2/v1/token"
    # token_uri = "http://localhost:8888"
    headers = {"Content-Type": "application/x-www-form-urlencoded"}
    params = {
        "code": code,
        "client_id": current_app.config["ELYBY_CLIENT_ID"],
        "client_secret": current_app.config["ELYBY_CLIENT_SECRET"],
        "redirect_uri": current_app.config["REDIRECT_URI"],
        "grant_type": "authorization_code",
    }
    res = requests.post(token_uri, data=params, headers=headers)
    res_dict = res.json()
    if res_dict.get("token_type") == "Bearer":
        access_token = res_dict.get("access_token")
        return access_token
    return None


def get_raw_data(access_token):
    info_uri = "https://account.ely.by/api/account/v1/info"
    headers = {
        "Content-Type": "application/x-www-form-urlencoded",
        "Authorization": f"Bearer {access_token}",
    }
    res = requests.post(info_uri, headers=headers)
    res_dict = res.json()
    return res_dict
