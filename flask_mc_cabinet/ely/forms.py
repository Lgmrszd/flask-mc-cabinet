from flask_wtf import FlaskForm
from wtforms import SubmitField


class RegisterForm(FlaskForm):
    submit = SubmitField("Register!")
