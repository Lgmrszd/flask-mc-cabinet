import os

from environs import Env

env = Env()
env.read_env()


class Config:
    def __init__(self, instance_path):
        self.FIRST_RUN = env.bool("FIRST_RUN", default=True)
        self.DEBUG = env.bool("FLASK_DEBUG", default=False)
        self.SECRET_KEY = env.str("SECRET_KEY")
        self.SQLALCHEMY_DATABASE_URI = env.str(
            "DATABASE_URL",
            default="sqlite:///"
            + os.path.join(instance_path, "flask_mc_cabinet.db"),
        )
        self.SQLALCHEMY_TRACK_MODIFICATIONS = False

        self.ELYBY_CLIENT_ID = env.str("ELYBY_CLIENT_ID")
        self.ELYBY_CLIENT_SECRET = env.str("ELYBY_CLIENT_SECRET")
        self.REDIRECT_URI = env.str("REDIRECT_URI")
