from flask import Blueprint, request, jsonify

from flask_mc_cabinet.extensions import db
from flask_mc_cabinet.public.models import User

bp_gravit = Blueprint(
    "gravit", __name__, url_prefix="/gravit"
)


@bp_gravit.route("/provider", methods=["GET", "POST"])
def provider():
    post_data = request.get_json()
    username = post_data.get("username")

    user: User = User.query.filter_by(username=username).first()

    if not user:
        return jsonify({"error": f"User {username} not found"})

    if user.check_password(post_data.get("password")):
        return jsonify(
            {
                "username": username,
                "permissions": user.permissions
            }
        )

    return jsonify({"error": "Wrong password"})


@bp_gravit.route("/handler/getUrl", methods=["GET", "POST"])
def handler_get():
    post_data = request.get_json()

    username = post_data.get("username")
    uuid = post_data.get("uuid")
    if not (username or uuid):
        return jsonify({"error": "Wrong request"})

    if username:
        user: User = User.query.filter_by(username=username).first()
    else:
        user: User = User.query.filter_by(uuid=uuid).first()

    if not user:
        return jsonify({"error": f"User {username} not found"})

    data = {
        "username": user.username,
        "uuid": user.uuid,
        "accessToken": user.access_token or "null",
        "serverId": user.server_id or "null",
    }
    return jsonify(data)


@bp_gravit.route("/handler/updateAuthUrl", methods=["GET", "POST"])
def handler_update_auth():
    post_data = request.get_json()

    username = post_data.get("username")
    uuid = post_data.get("uuid")
    access_token = post_data.get("access_token")
    if not (username or uuid):
        return jsonify({"error": "Wrong request"})

    user: User = User.query.filter_by(username=username, uuid=uuid).first()

    if not user:
        return jsonify({"error": f"User {username} not found"})

    user.access_token = access_token
    db.session.add(user)
    db.session.commit()

    data = {
        "success": True
    }
    return jsonify(data)


@bp_gravit.route("/handler/updateServerIdUrl", methods=["GET", "POST"])
def handler_update_server_id():
    post_data = request.get_json()

    uuid = post_data.get("uuid")
    server_id = post_data.get("server_id")
    if not uuid:
        return jsonify({"error": "Wrong request"})

    user: User = User.query.filter_by(uuid=uuid).first()

    if not user:
        return jsonify({"error": f"User with {uuid} not found"})

    user.server_id = server_id
    db.session.add(user)
    db.session.commit()

    data = {
        "success": True
    }
    return jsonify(data)
