import os
import click

from flask.cli import with_appcontext

HERE = os.path.abspath(os.path.dirname(__file__))
PROJECT_ROOT = os.path.join(HERE, os.pardir)


@click.command()
@with_appcontext
def check_app():
    # TODO: add actual checks
    print("Nothing to check")
