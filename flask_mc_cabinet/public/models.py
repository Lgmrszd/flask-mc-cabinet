from __future__ import annotations

from datetime import datetime

from flask_mc_cabinet.extensions import db
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash

from flask_mc_cabinet.extensions import login_manager


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    uuid = db.Column(db.String(48), index=True, unique=True)
    active = db.Column(db.Boolean, default=False)
    # email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    # Gravit specific
    permissions = db.Column(db.Integer, default=0)
    access_token = db.Column(db.String(32))
    server_id = db.Column(db.String(48))

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password_valid(self):
        if self.password_hash.startswith("INVALID:"):
            return False, self.password_hash[len("INVALID:"):]
        return True, None

    def check_password(self, password):
        return self.check_password_valid() and check_password_hash(
            self.password_hash, password
        )

    def __repr__(self):
        return "<User {}>".format(self.username)


@login_manager.user_loader
def load_user(id):
    return User.query.get(int(id))


class ElyData(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=False)
    uuid = db.Column(db.String(48), index=True, unique=True)
    username = db.Column(db.String(64))
    access_token = db.Column(db.String(300))
    profile_link = db.Column(db.String(30))
    last_updated = db.Column(db.DateTime)

    user_id = db.Column(db.Integer, db.ForeignKey(User.id))
    user = db.relationship(User, backref=db.backref("ely_data", uselist=False))

    @classmethod
    def from_raw_data(cls, raw_data) -> ElyData:
        ely_data = cls(
            id=raw_data["id"],
            uuid=raw_data["uuid"],
            username=raw_data["username"],
            profile_link=raw_data["profileLink"],
            last_updated=datetime.now(),
        )
        db.session.add(ely_data)
        db.session.commit()
        return ely_data
