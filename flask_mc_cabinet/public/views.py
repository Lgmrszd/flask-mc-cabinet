from flask import Blueprint, render_template, url_for, flash, redirect
from flask_login import current_user, login_user, logout_user, login_required

from flask_mc_cabinet.extensions import db
from .forms import LoginForm, ChangePasswordForm
from .models import User

blueprint = Blueprint("public", __name__)


@blueprint.route("/")
@blueprint.route("/index")
def index():
    return render_template("index.html", current="index")


@blueprint.route("/profile")
@login_required
def profile():
    return render_template("profile.html", current="profile")


@blueprint.route("/login", methods=["GET", "POST"])
def login():
    if current_user.is_authenticated:
        return redirect(url_for(".index"))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash("Invalid username or password", category="error")
            return redirect(url_for(".login"))
        login_user(user, remember=False)
        return redirect(url_for(".index"))
    return render_template("login.html", form=form)


@blueprint.route("/change_password", methods=["GET", "POST"])
@login_required
def change_password():
    form = ChangePasswordForm()
    if form.validate_on_submit():
        user: User = current_user
        user.set_password(form.new_password.data)
        db.session.add(user)
        db.session.commit()
        flash("Successfully changed the password!", category="message")
        return redirect(url_for(".index"))
    return render_template("change_password.html", form=form)


@blueprint.route("/logout")
def logout():
    logout_user()
    flash("Logged out!", category="message")
    return redirect(url_for(".index"))


@blueprint.before_request
def validate_user():
    # TODO: check if user is not active and warn
    # flash(request.endpoint)
    pass


@blueprint.context_processor
def utility_processor():
    def nav_links(current=None):
        links = [
            {
                "fullname": "Main Page",
                "endpoint": "public.index",
                "url": url_for(".index"),
            }
        ]
        current_user.is_authenticated and links.append(
            {
                "fullname": current_user.username,
                "endpoint": "public.profile",
                "url": url_for(".profile"),
            }
        )
        links.append(
            {
                "fullname": "Logout",
                "endpoint": "public.logout",
                "url": url_for(".logout"),
                "classes": [],
            }
            if current_user.is_authenticated
            else {
                "fullname": "Login",
                "endpoint": "public.login",
                "url": url_for(".login"),
            }
        )
        # if current:
        #     for link in links:
        #         if link["alias"] == current:
        #             link["current"] = True

        return links

    return dict(nav_links=nav_links)
