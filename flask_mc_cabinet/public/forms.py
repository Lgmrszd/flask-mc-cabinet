from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import DataRequired, EqualTo


class LoginForm(FlaskForm):
    username = StringField("Username", validators=[DataRequired()])
    password = PasswordField("Password", validators=[DataRequired()])
    submit = SubmitField("Sign In")


class ChangePasswordForm(FlaskForm):
    new_password = PasswordField("New password", validators=[DataRequired()])
    new_password_2 = PasswordField(
        "Repeat new password",
        validators=[DataRequired(), EqualTo("new_password")],
    )
    submit = SubmitField("Change password")
